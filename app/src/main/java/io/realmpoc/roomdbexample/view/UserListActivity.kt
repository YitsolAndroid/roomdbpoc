package io.realmpoc.roomdbexample.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.yits.allshop.utility.LinearLayoutSpaceItemDecoration
import io.realmpoc.roomdbexample.R
import io.realmpoc.roomdbexample.adapter.UserDataAdapter
import io.realmpoc.roomdbexample.database.table.Test
import io.realmpoc.roomdbexample.database.table.User
import io.realmpoc.roomdbexample.model.OLDataModel
import io.realmpoc.roomdbexample.utility.IDataListener
import io.realmpoc.roomdbexample.utility.LoggerUtils
import io.realmpoc.roomdbexample.viewmodel.UserListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class UserListActivity : AppCompatActivity(),IDataListener {

    override fun getAllOnlineShopDataList(datalist: MutableList<OLDataModel>?) {
        LoggerUtils.error(UserListActivity::class.simpleName,"online shop catg data size is "+datalist?.size)
    }

    private var viewmodel:UserListViewModel?=null
    private var _userAdapter:UserDataAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LoggerUtils.error(UserListActivity::class.simpleName,"onCreate called")
        fab.setOnClickListener(View.OnClickListener {
            val addIntent=Intent(this,AddUserActivity::class.java)
            startActivity(addIntent)
        })

        setUserListAdapter(ArrayList<User>())
        viewmodel = ViewModelProviders.of(this).get(UserListViewModel::class.java)

        viewmodel!!.getAllUserDataList().observe(this,object :Observer<List<User>>
        {
            override fun onChanged(userlist: List<User>?) {
                _userAdapter?.refreshAdapter(userlist!!)
            }

        })
        viewmodel!!.getAllTestData().observe(this,object :Observer<List<Test>>
        {
            override fun onChanged(testdatalist: List<Test>?) {
                LoggerUtils.error(UserListActivity::class.simpleName,"test table data is "+ testdatalist!!.size)
            }
        })



    }

    override fun onStart() {
        super.onStart()
        LoggerUtils.error(UserListActivity::class.simpleName,"onStart called")
    }

    override fun onStop() {
        super.onStop()
        LoggerUtils.error(UserListActivity::class.simpleName,"onStop called")

    }

    override fun onRestart() {
        super.onRestart()
        LoggerUtils.error(UserListActivity::class.simpleName,"onRestart called")
    }

    override fun onResume() {
        super.onResume()
        LoggerUtils.error(UserListActivity::class.simpleName,"onResume called")
    }

    override fun onPause() {
        super.onPause()
        LoggerUtils.error(UserListActivity::class.simpleName,"onPause called")
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        LoggerUtils.error(UserListActivity::class.simpleName,"config change called")
    }

    private fun setUserListAdapter(arrayList: ArrayList<User>) {
        rcly_userlist.layoutManager=LinearLayoutManager(getActvContext(),LinearLayout.VERTICAL,false)
        rcly_userlist.addItemDecoration(LinearLayoutSpaceItemDecoration(4))
        _userAdapter= UserDataAdapter(getActvContext(),arrayList)
        rcly_userlist.adapter=_userAdapter

    }

    fun getActvContext():Context
    {
        return UserListActivity@this
    }
}
