package io.realmpoc.roomdbexample.view

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AppCompatActivity
import android.view.View
import io.realmpoc.roomdbexample.R
import io.realmpoc.roomdbexample.database.table.User
import io.realmpoc.roomdbexample.utility.AppConstant
import io.realmpoc.roomdbexample.viewmodel.UserListViewModel
import kotlinx.android.synthetic.main.activity_add_user.*
import java.util.*

class AddUserActivity : AppCompatActivity() {

    private var viewModel :UserListViewModel?=null
    private var _userdata:User?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)
        viewModel=ViewModelProviders.of(this).get(UserListViewModel::class.java)
        btn_saveShop.setOnClickListener(View.OnClickListener {
            if(validateUserData(edt_firstName,edt_lastName,edt_userAge))
            {
                val sex_type=getUserSexType(rad_male.isChecked,rad_female.isChecked)
                _userdata=User(getEnteredData(edt_firstName),getEnteredData(edt_lastName),getEnteredData(edt_userAge).toInt()
                ,sex_type,Date())
                viewModel?.insertUser(_userdata)
                clearAllField()
                gotoUserListScreen()
            }
        })

    }

    private fun getUserSexType(rad_male_status: Boolean, rad_female_status: Boolean): Int {
        if(rad_female_status)
        {
            return AppConstant.FEMALE_TYPE
        }
        else
        {
            return AppConstant.MALE_TYPE
        }
    }

    private fun gotoUserListScreen() {
        val userlistIntent= Intent(getActContext(),UserListActivity::class.java)
        startActivity(userlistIntent)
    }

    private fun clearAllField() {
        edt_firstName.setText("")
        edt_lastName.setText("")
        edt_userAge.setText("")
    }

    private fun validateUserData(edt_firstName: TextInputEditText?,
                                 edt_lastName: TextInputEditText?,
                                 edt_userAge: TextInputEditText?): Boolean
    {
        if(getEnteredData(edt_firstName).equals("")&&getEnteredData(edt_firstName).length==0
        &&getEnteredData(edt_firstName).isEmpty())
        {
            edt_firstName?.error="First name is required"
            return false
        }
        else if(getEnteredData(edt_lastName).equals("")&&getEnteredData(edt_firstName).length==0
        &&getEnteredData(edt_lastName).isEmpty())
        {
            edt_lastName?.error="Last name is required"
            return false
        }
        else if(getEnteredData(edt_userAge).equals("")&&getEnteredData(edt_userAge).length==0
        &&getEnteredData(edt_userAge).isEmpty())
        {
            edt_userAge?.error="User age is required"
            return false
        }
        else
        {
            return true
        }
    }

    private fun getEnteredData(edt_userview: TextInputEditText?): String {
        return edt_userview?.text.toString().trim()
    }

    public fun getActContext(): Context
    {
        return AddUserActivity@this
    }
}
