package io.realmpoc.roomdbexample.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.realmpoc.roomdbexample.database.table.Test;
import io.realmpoc.roomdbexample.database.table.User;
import io.realmpoc.roomdbexample.model.OLDataModel;
import io.realmpoc.roomdbexample.repository.UserRepository;
import io.realmpoc.roomdbexample.utility.IAllOLCatgDataListener;
import io.realmpoc.roomdbexample.utility.IDataListener;
import io.realmpoc.roomdbexample.utility.LoggerUtils;

/**
 * Created by yitsol on 09-03-2018.
 */

public class UserListViewModel extends AndroidViewModel
{
    private UserRepository userRepository;
    private LiveData<List<User>> alluserlist;
    private LiveData<List<Test>> alltestdatalist;
    private IDataListener listener;

    public List<OLDataModel> getAllOlCatglist() {
        return allOlCatglist;
    }

    private List<OLDataModel> allOlCatglist;

    public LiveData<List<User>> getAllUserDataList() {
        return alluserlist;
    }

    public LiveData<List<Test>> getAllTestData()
    {
        return alltestdatalist;
    }

    public UserListViewModel(@NonNull Application application) {
        super(application);
        allOlCatglist=new ArrayList<>();
        userRepository=new UserRepository(application);
        alluserlist=userRepository.getUserdatalist();
        alltestdatalist=userRepository.getAllTestDataResult();
        userRepository.registerListener(new IAllOLCatgDataListener.OnGetAllOLCatgDataCallback() {
            @Override
            public void onSuccess(List<OLDataModel> datalist) {
                allOlCatglist=datalist;
                LoggerUtils.error(UserListViewModel.class.getSimpleName(),"all catg list size is "+allOlCatglist.size());
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    public void insertUser(User user)
    {
        userRepository.insertUserData(user);
    }

    public void delete(User user)
    {
        userRepository.deleteUser(user);
    }

    public void closeDataBase()
    {
        userRepository.closeOpenedDatabase();
    }


}
