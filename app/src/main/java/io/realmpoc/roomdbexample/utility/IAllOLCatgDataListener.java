package io.realmpoc.roomdbexample.utility;

import java.util.List;

import io.realmpoc.roomdbexample.model.OLDataModel;

/**
 * Created by yitsol on 12-03-2018.
 */

public interface IAllOLCatgDataListener
{
    interface OnGetAllOLCatgDataCallback
    {
        void onSuccess(List<OLDataModel> datalist);
        void onError(String message);
    }
}
