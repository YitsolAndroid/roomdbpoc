package io.realmpoc.roomdbexample.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.realmpoc.roomdbexample.database.table.Test;
import io.realmpoc.roomdbexample.database.table.User;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by yitsol on 08-03-2018.
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    LiveData<List<User>> getAllUser();

    @Query("SELECT * FROM user where first_name LIKE  :firstName AND last_name LIKE :lastName")
    User findByName(String firstName, String lastName);

    @Query("SELECT * from user where uid = :id")
    User getItembyId(String id);

    @Query("SELECT COUNT(*) from user")
    int countUsers();

    @Insert(onConflict = REPLACE)
    void insertAll(User... users);

    @Delete
    void deleteUser(User user);

    @Query("DELETE FROM user")
    void deleteAllUser();

    @Query("DELETE FROM test")
    void deleteAllTest();

    @Insert(onConflict = REPLACE)
    void insertAll(Test... test);

    @Query("SELECT * FROM test")
    LiveData<List<Test>> getAllTestData();
}
