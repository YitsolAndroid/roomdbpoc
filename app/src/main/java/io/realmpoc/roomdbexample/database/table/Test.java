package io.realmpoc.roomdbexample.database.table;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by yitsol on 20-03-2018.
 */
@Entity(tableName = "test")
public class Test {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "testname")
    private String testName;

    public Test(String testName) {
        this.testName = testName;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}
