package io.realmpoc.roomdbexample.database.dbconfig;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.Date;

import io.realmpoc.roomdbexample.database.dao.UserDao;
import io.realmpoc.roomdbexample.database.table.Test;
import io.realmpoc.roomdbexample.database.table.User;
import io.realmpoc.roomdbexample.utility.LoggerUtils;

/**
 * Created by yitsol on 08-03-2018.
 */
@Database(entities = {User.class,Test.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract UserDao userDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "user-database")
                                    // allow queries on the main thread.
                                    // Don't do this on a real app! See PersistenceBasicSample for an example.
                                    .fallbackToDestructiveMigration()
                                    .addCallback(userdatacallback)
                                    .build();
                }
            }
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    private static AppDatabase.Callback userdatacallback=new AppDatabase.Callback()
    {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            LoggerUtils.error(AppDatabase.class.getSimpleName(),"database got opened");
            new AddInitUserDataTask(INSTANCE).execute();
        }

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            LoggerUtils.error(AppDatabase.class.getSimpleName(),"database got created");
        }
    };

    private static class AddInitUserDataTask extends AsyncTask<Void, Void, Void> {
        private UserDao userDao;

        AddInitUserDataTask(AppDatabase appDatabase) {
            userDao=appDatabase.userDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userDao.deleteAllUser();
            userDao.deleteAllTest();
            User user1=new User("Rahul","Kumar",28,1,new Date());
            userDao.insertAll(user1);
            user1=new User("Ravi","Kumar",30,1,new Date());
            userDao.insertAll(user1);
            Test test1=new Test("test1 value");
            userDao.insertAll(test1);
            test1=new Test("test 2 value");
            userDao.insertAll(test1);
            LoggerUtils.error(AppDatabase.class.getSimpleName(),"initial data added");
            return null;
        }
    }


}
