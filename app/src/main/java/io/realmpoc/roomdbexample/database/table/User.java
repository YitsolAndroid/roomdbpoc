package io.realmpoc.roomdbexample.database.table;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

import io.realmpoc.roomdbexample.utility.DateConverter;

/**
 * Created by yitsol on 08-03-2018.
 */
@Entity(tableName = "user")
public class User {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "age")
    private int age;

    @ColumnInfo(name="sex")
    private int sex_type;

    @TypeConverters(DateConverter.class)
    private Date currsysdate;

    public User(String firstName, String lastName, int age, int sex_type, Date currsysdate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.sex_type = sex_type;
        this.currsysdate = currsysdate;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getCurrsysdate() {
        return currsysdate;
    }

    public void setCurrsysdate(Date currsysdate) {
        this.currsysdate = currsysdate;
    }

    public int getSex_type() {
        return sex_type;
    }

    public void setSex_type(int sex_type) {
        this.sex_type = sex_type;
    }

    public String getFullUserName()
    {
        if(sex_type==1)
        {
            return "Mr." +firstName + " " +lastName;
        }
        else
        {
            return "Mrs."+firstName + " " +lastName;
        }
    }
}
