package io.realmpoc.roomdbexample.service;

/**
 * Created by yitsol on 12-03-2018.
 */

public class ApiUtils
{
    private ApiUtils() {}

    public static final String BASE_URL="http://45.79.209.142:9999/api/";

    public static APIService getAPIService() {
        return RetrofitClient.
                getClient(BASE_URL).
                create(APIService.class);
    }
}
