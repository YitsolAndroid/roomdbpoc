package io.realmpoc.roomdbexample.service;

import io.realmpoc.roomdbexample.model.OLCatgModel;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by yitsol on 12-03-2018.
 */

public interface APIService
{
    @GET("ONCategories")
    Call<OLCatgModel> getAllOnlineCatg();
}
