package io.realmpoc.roomdbexample.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realmpoc.roomdbexample.R
import io.realmpoc.roomdbexample.database.table.User
import kotlinx.android.synthetic.main.row_userlayout.view.*


/**
 * Created by yitsol on 09-03-2018.
 */
class UserDataAdapter(_context: Context, datalist:List<User>): RecyclerView.Adapter<UserDataAdapter.ViewHolder>()
{
    var _context:Context?=null
    var datalist:List<User>?=null

    init {
        this._context=_context;
        this.datalist=datalist;
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val userview=LayoutInflater.from(_context).inflate(R.layout.row_userlayout,parent,false)
        return ViewHolder(userview)
    }

    override fun getItemCount(): Int {
        return this.datalist!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindItems(this.datalist!!.get(position),position)
    }

    fun refreshAdapter(userlist:List<User>)
    {
        this.datalist=userlist
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)
    {
        fun bindItems(userdata: User, position: Int) {
            itemView.txt_userfullname.setText(userdata.fullUserName)
            itemView.txt_userage.setText(userdata.age.toString())
            itemView.txt_currdatetime.setText(userdata.currsysdate.toString())
        }
    }


}