package io.realmpoc.roomdbexample.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.util.List;

import io.realmpoc.roomdbexample.database.dao.UserDao;
import io.realmpoc.roomdbexample.database.dbconfig.AppDatabase;
import io.realmpoc.roomdbexample.database.table.Test;
import io.realmpoc.roomdbexample.database.table.User;
import io.realmpoc.roomdbexample.model.OLCatgModel;
import io.realmpoc.roomdbexample.service.APIService;
import io.realmpoc.roomdbexample.service.ApiUtils;
import io.realmpoc.roomdbexample.utility.IAllOLCatgDataListener;
import io.realmpoc.roomdbexample.utility.LoggerUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yitsol on 12-03-2018.
 */

public class UserRepository
{
    private UserDao userDao;
    private LiveData<List<User>> userdatalist;
    private LiveData<List<Test>> testdatalist;
    private AppDatabase appDatabase;
    private IAllOLCatgDataListener.OnGetAllOLCatgDataCallback dataListener;

    private APIService apiService;

    public LiveData<List<User>> getUserdatalist() {
        return userdatalist;
    }

    public LiveData<List<Test>> getAllTestDataResult(){
        return testdatalist;
    }

    public UserRepository(Application application)
    {
        apiService= ApiUtils.getAPIService();
        getAllOnlineCatgList();
        appDatabase=AppDatabase.getAppDatabase(application);
        userDao=appDatabase.userDao();
        userdatalist=userDao.getAllUser();
        testdatalist=userDao.getAllTestData();
    }

    public void registerListener(IAllOLCatgDataListener.OnGetAllOLCatgDataCallback listener)
    {
        this.dataListener=listener;
    }

    public void insertUserData(User user)
    {
        new InsertUserAsyncTask(userDao).execute(user);
    }

    public void deleteUser(User usermodel)
    {
        new DeleteAsyncTask(userDao).execute(usermodel);
    }


    private static class InsertUserAsyncTask extends AsyncTask<User,Void,Void>
    {
        private UserDao insertUserDao;

        InsertUserAsyncTask(UserDao insertUserDao) {
            this.insertUserDao = insertUserDao;
        }

        @Override
        protected Void doInBackground(User... users) {
            insertUserDao.insertAll(users[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<User,Void,Void>
    {

        private UserDao deleteUserDao;

        DeleteAsyncTask(UserDao deleteUserDao) {
            this.deleteUserDao = deleteUserDao;
        }

        @Override
        protected Void doInBackground(User... users) {
            deleteUserDao.deleteUser(users[0]);
            return null;
        }
    }

    public void closeOpenedDatabase()
    {
        if(appDatabase.isOpen())
        {
            appDatabase.close();
            AppDatabase.destroyInstance();
        }
    }

    private void getAllOnlineCatgList()
    {
        LoggerUtils.error(UserRepository.class.getSimpleName(),"service call started");
        apiService.getAllOnlineCatg().enqueue(new Callback<OLCatgModel>() {
            @Override
            public void onResponse(Call<OLCatgModel> call, Response<OLCatgModel> response) {
                LoggerUtils.error(UserRepository.class.getSimpleName(),"service call response code "+response.code());
                if(response.code()== HttpURLConnection.HTTP_OK)
                {
                    dataListener.onSuccess(response.body().getData());
                }
                else
                {
                    LoggerUtils.error(UserRepository.class.getSimpleName(),"error on service call");
                }
            }

            @Override
            public void onFailure(Call<OLCatgModel> call, Throwable t) {

            }
        });
    }

}
